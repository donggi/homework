# LIGS python study HW1


# Q1.
# 12개의 수를 값으로 갖는 list를 입력받은 후,
# 2 번재로 큰 숫자가 첫 번째
# 4 번째로 작은 숫자가 뒤에서 두 번째
# 두 수를 제외한 나머지 수 10개 중 작은 숫자 5개는 오름차순, 큰 숫자 5개는 내림차순으로 정렬
nums = [int(i) for i in input('공백을 구분자로 12개의 숫자를 입력하세요: ').split(' ')]
nums = nums.sort() # 오름차순으로 정렬
snd = nums.pop(-2) # 2번째로 큰 숫자
fth = nums.pop(3) # 4번째로 작은 숫자
nums.insert(0, snd) # 2번째로 작은 숫자를 첫 번째에 삽입
post = nums[6:12] # 입력받은 숫자 중에서 6번째 부터 11번째 숫자
post.reverse() # 뒤집기
post.insert(-1, fth) # 4 번째로 작은 숫자를 뒤에서 두 번째에 삽입
del nums[6:] # 6번째 이후의 숫자 삭제
nums = nums + post # 하나의 list로 합치기
print(nums)

# 출력결과: 0 1 2 3 4 5 6 7 8 9 10 11 입력 시
# [10, 0, 1, 2, 4, 5, 11, 9, 8, 7, 3, 6] 출력 됨
