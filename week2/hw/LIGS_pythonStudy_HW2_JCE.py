# LIGS Python study HW2

# Q1. What is the result?
a = "Life is too short, you need python"

if "wife" in a:
    print("wife")
elif "python" in a and "you" not in a:
    print("python")
elif "shirt" not in a:
    print("shirt")
elif "need" in a:
    print("need")
else:
    print("none")
    


# Q2. 입력으로 들어오는 모든 수의 평균 값을 계산해 주는 함수를 작성해 보자.
# 또한 모든 수와 평균을 'result.txt' 파일로 저장하자.
# (단, 입력으로 들어오는 수의 개수는 정해져 있지 않다.)
def MEAN (*prm):
    qnt = len(prm) # 입력으로 들어오는 모든 수의 평균을 계산.
    sms = sum(prm)
    cal = sms/qnt
    f = open('C:/Users/USER/Desktop/result.txt', 'w') # 모든 수와 평균을 txt 파일에 저장.
    data = '입력 값: ' + str(prm) +', '+ '평균 값: ' + str(cal)
    f.write(data) # write() 함수는 str만 입력값으로 받는다.
    f.close() # 파일을 open()한 후에는 close() 해주어야한다.
    return cal

print(MEAN(5,5))

