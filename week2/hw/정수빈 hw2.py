# Q1 결과 보기
a = "Life is too short, you need python"

if "wife" in a:
    print("wife")

elif "python" in a and "you" not in a:
    print("python")

elif "shirt" not in a:
    print("shirt")

elif "need" in a:
    print("need")

else:
    print("none")




# Q2 평균값 계산 함수 만들기
num = input("숫자를 입력하세요")          # 입력받음
num2 = num.split()                       # 공백 기준 분리
numbers = list(map(int,num2))            # int형 처리

sum = 0                                  # 받은 데이터 합
for i in numbers:
    sum += i

def Average(numbers):                    # 데이터 평균 함수
    average = sum/len(numbers)
    return average
print(Average(numbers))

# 파일 저장
f = open("result.txt", 'w')

data_1 = "숫자 : "
data1 = num
data_2 = " 평균값 : "
data2 = str(Average(numbers))

f.write(data_1)
f.write(data1)
f.write(data_2)
f.write(data2)
f.close()
